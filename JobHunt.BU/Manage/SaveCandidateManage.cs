﻿using JobHunt.BU.ConvertData;
using JobHunt.BU.DTO;
using JobHunt.Model.EF;
using JobHunt.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JobHunt.BU.Manage
{
    public class SaveCandidateManage
    {
        ConvertDataSaveCandidate convertData = new ConvertDataSaveCandidate();
        //private JobHuntEntities db = null;
        JobHuntRepository<SaveCandidate> repo = new JobHuntRepository<SaveCandidate>();
        public bool Insert(SaveCandidateDTO DTO)
        {
            try
            {
                repo.Insert(convertData.ConvertToEF(DTO));
                repo.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }


        public bool Delete(SaveCandidateDTO DTO)
        {
            try
            {
                repo.Delete(DTO.SaveCandidateId);
                repo.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
